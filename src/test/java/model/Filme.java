package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Filme {

    private Integer codigo;
    private String nome;
    private Integer anoLancamento;
    private String sinopse;
    private Integer faixaEtaria;
    private String genero;

    public Filme(Integer codigo, String nome, Integer anoLancamento, String sinopse, Integer faixaEtaria, String genero) {
        this.codigo = codigo;
        this.nome = nome;
        this.anoLancamento = anoLancamento;
        this.sinopse = sinopse;
        this.faixaEtaria = faixaEtaria;
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "{\n" +
                "    \"codigo\": "+ codigo +",\n" +
                "    \"nome\": \""+ nome +"\",\n" +
                "    \"anoLancamento\": "+ anoLancamento +",\n" +
                "    \"sinopse\": \""+ sinopse +"\",\n" +
                "    \"faixaEtaria\": "+ faixaEtaria +",\n" +
                "    \"genero\": \""+ genero +"\"\n" +
                "  }";
    }


}
