package step_definitions;

import com.github.javafaker.Faker;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.response.ValidatableResponse;
import model.Filme;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import support.enums.ApiPath;
import support.enums.BaseUri;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Clock;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

import static support.utils.RestContext.*;

public class FilmeStepDefinitions {

    ValidatableResponse response;

    Faker teste = new Faker();

    String nome = teste.name().title();
    String sinopse = teste.name().lastName();
    Integer faixaEtaria = teste.number().numberBetween(10, 65);
    Integer numero = teste.number().numberBetween(4, 100);
    String genero = teste.animal().name();
    Integer anoLancamento = teste.number().numberBetween(1820, 2023);

    @Quando("faco uma requisicao para a url de filmes")
    public void faco_uma_requisicao_para_a_url_de_filmes() {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.GET_FILMES.getPath());
        getRequest();
        response = getResponse().then().log().all();
    }

    @Entao("valido se a resposta")
    public void valido_se_a_resposta() throws IOException, JSONException {

        // Podemos validar a resposta desse formato campo por campo.
//        assertEquals(Integer.valueOf(1), response.extract().path("[0].codigo"));
//        assertEquals("Pesadelo na Cozinha", response.extract().path("[0].nome"));
//        assertEquals(Integer.valueOf(2018), response.extract().path("[0].anoLancamento"));
//        assertEquals("documentario sobre restaurantes", response.extract().path("[0].sinopse"));
//        assertEquals(Integer.valueOf(10), response.extract().path("[0].faixaEtaria"));
//        assertEquals("documentario", response.extract().path("[0].genero"));
//
//        assertEquals(Integer.valueOf(2), response.extract().path("[1].codigo"));
//        assertEquals("Pesadelo na Cozinha 2", response.extract().path("[1].nome"));
//        assertEquals(Integer.valueOf(2020), response.extract().path("[1].anoLancamento"));
//        assertEquals("documentario sobre restaurantes", response.extract().path("[1].sinopse"));
//        assertEquals(Integer.valueOf(10), response.extract().path("[1].faixaEtaria"));
//        assertEquals("documentario", response.extract().path("[1].genero"));
//
//        assertEquals(Integer.valueOf(3), response.extract().path("[2].codigo"));
//        assertEquals("Pesadelo na Cozinha 3", response.extract().path("[2].nome"));
//        assertEquals(Integer.valueOf(2021), response.extract().path("[2].anoLancamento"));
//        assertEquals("documentario sobre restaurantes", response.extract().path("[2].sinopse"));
//        assertEquals(Integer.valueOf(10), response.extract().path("[2].faixaEtaria"));
//        assertEquals("documentario", response.extract().path("[2].genero"));

        //Ou podemos validar via json, como formato abaixo.
        //true e false true valida todos os campos, false so vai validar o que tenho no json.
        String jsonParaValidar  = new String(Files.readAllBytes(Paths.get("src/test/resources/jsons/responseFilmes.json")));
        JSONAssert.assertEquals(jsonParaValidar, response.extract().asString(), true);
    }

    @Quando("faco uma requisicao para a url de filme passando o codigo")
    public void faco_uma_requisicao_para_a_url_de_filme_passando_o_codigo() {
        initRequest();
        //aqui em cima e repassado como http://localhost:8080/filme/{codigo}
        //http://localhost:8080/filme/1
        //http://localhost:8080/filme?TESTE=1234
        //http://localhost:8080/filme/5?context=json
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.GET_FILME.getPath());
        Map<String, String> params = new HashMap<String, String>();
        params.put("codigo", "1");
        setPathParams(params);

        getRequest();
        response = getResponse().then().log().all();
    }
    @Entao("valido se a resposta de um filme")
    public void valido_se_a_resposta_de_um_filme() {
        assertEquals(Integer.valueOf(1), response.extract().path("codigo"));
        assertEquals("Pesadelo na Cozinha", response.extract().path("nome"));
        assertEquals(Integer.valueOf(2018), response.extract().path("anoLancamento"));
        assertEquals("documentario sobre restaurantes", response.extract().path("sinopse"));
        assertEquals(Integer.valueOf(10), response.extract().path("faixaEtaria"));
        assertEquals("documentario", response.extract().path("genero"));
    }

    @Quando("faco uma requisicao para deletar um filmes")
    public void faco_uma_requisicao_para_deletar_um_filmes() {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.DELETE_FILME.getPath());
        Map<String, String> params = new HashMap<String, String>();
        params.put("codigo", "3");
        setPathParams(params);

        deleteRequest();
        response = getResponse().then().log().all();
    }
    @Entao("valido se o filme foi deletado")
    public void valido_se_o_filme_foi_deletado() {
        response.statusCode(200);
    }

    @Quando("faco uma requisicao para criar um filme")
    public void faco_uma_requisicao_para_criar_um_filme() throws IOException {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.POST_CRIAR_FILME.getPath());
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        setHeaders(headers);

        //Usando arquivo json
//        String jsonParaCriar  = new String(Files.readAllBytes(Paths.get("src/test/resources/jsons/responseFilmes.json")));
//        setBody(jsonParaCriar);

        //Passando json direto no body
//        setBody("{\n" +
//                "    \"codigo\": 4,\n" +
//                "    \"nome\": \"Pesadelo na Cozinha 4\",\n" +
//                "    \"anoLancamento\": 2023,\n" +
//                "    \"sinopse\": \"documentario sobre restaurantes\",\n" +
//                "    \"faixaEtaria\": 12,\n" +
//                "    \"genero\": \"documentario\"\n" +
//                "  }");

        Filme bodyOnjeto = new Filme(numero, nome, anoLancamento, sinopse, faixaEtaria,genero);
        setBody(bodyOnjeto.toString());
        System.out.println(bodyOnjeto.toString());
        postRequest();
        response = getResponse().then().log().all();


    }
    @Entao("valido se o filme foi criado")
    public void valido_se_o_filme_foi_criado() {
        assertEquals(numero, response.extract().path("codigo"));
        assertEquals(nome, response.extract().path("nome"));
        assertEquals(anoLancamento, response.extract().path("anoLancamento"));
        assertEquals(sinopse, response.extract().path("sinopse"));
        assertEquals(faixaEtaria, response.extract().path("faixaEtaria"));
        assertEquals(genero, response.extract().path("genero"));
    }

    @Quando("faco uma requisicao para editar um filme")
    public void faco_uma_requisicao_para_editar_um_filme() {
        initRequest();
        setPath(BaseUri.BASE_URI.getPath(), ApiPath.EDITAR_FILME.getPath());

        // /Filme/{codigo} passo que a variavel seja um numero que escolhir , entao vai ficar > filma/3
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        setHeaders(headers);


        Map<String, String> params = new HashMap<String, String>();
        params.put("codigo", "2");
        setPathParams(params);

        Filme bodyOnjeto = new Filme(2, nome, anoLancamento, sinopse, faixaEtaria,genero);
        setBody(bodyOnjeto.toString());
        putRequest();

        response = getResponse().then().log().all();

    }
    @Entao("valido se o filme foi editado")
    public void valido_se_o_filme_foi_editado() {
        assertEquals(Integer.valueOf(2), response.extract().path("codigo"));
        assertEquals(nome, response.extract().path("nome"));
        assertEquals(anoLancamento, response.extract().path("anoLancamento"));
        assertEquals(sinopse, response.extract().path("sinopse"));
        assertEquals(faixaEtaria, response.extract().path("faixaEtaria"));
        assertEquals(genero, response.extract().path("genero"));
    }
}
