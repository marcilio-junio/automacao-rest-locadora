package step_definitions;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import support.enums.ApiPath;
import support.enums.BaseUri;

import static org.junit.Assert.assertEquals;
import static support.utils.GetProperties.getProp;

public class StatusStepDefinitions {

    ValidatableResponse response;

    @Quando("faco uma requisicao para a url de status")
    public void faco_uma_requisicao_para_a_url_de_status() {
//        RestAssured
//                .given()
//                .when().get("http://localhost:8080/status")
//                .then().statusCode(200);

//                RestAssured
//                .given()
//                .when().get("http://localhost:8080/status")
//                .then().log().all();

//        response = RestAssured
//                .given()
//                .when().get(getProp("urlBase") + ApiPath.GET_STATUS)
//                .then().statusCode(200).log().all();

//        response = RestAssured
//                .given()
//                .when().get(getProp("urlBase") + ApiPath.GET_STATUS.getPath())
//                .then().statusCode(200).log().all();

        response = RestAssured
                .given()
                .when().get(BaseUri.BASE_URI.getPath() + ApiPath.GET_STATUS.getPath())
                .then().statusCode(200).log().all();


    }

    @Entao("valido se a resposta foi com status {string}")
    public void valido_se_a_resposta_foi_com_status(String string) {
        assertEquals(200, response.extract().statusCode());
        assertEquals(getProp("sucessoAplicacao"), response.extract().body().asPrettyString());
    }
}
