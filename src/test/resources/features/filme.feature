#language: pt
Funcionalidade: Filmes

  Cenario: Buscar Filme com Sucesso
    Quando faco uma requisicao para a url de filmes
    Entao valido se a resposta

  Cenario: Buscar Filme pelo Codigo
    Quando faco uma requisicao para a url de filme passando o codigo
    Entao valido se a resposta de um filme

  Cenario: Deletar um filme com sucesso
    Quando faco uma requisicao para deletar um filmes
    Entao valido se o filme foi deletado

  @criar
  Cenario: Criar um filme com sucesso
    Quando faco uma requisicao para criar um filme
    Entao valido se o filme foi criado

  @editar
  Cenario: Editar um filme com sucesso
    Quando faco uma requisicao para editar um filme
    Entao valido se o filme foi editado